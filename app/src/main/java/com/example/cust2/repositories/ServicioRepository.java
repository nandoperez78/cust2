package com.example.cust2.repositories;

// import android.app.Application;

// import androidx.annotation.WorkerThread;
import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.cust2.daos.ServicioDao;
import com.example.cust2.database.AppDatabase;
import com.example.cust2.entities.Servicio;

import java.util.List;

public class ServicioRepository {
    private ServicioDao servicioDao;

    private LiveData<List<Servicio>> servicios;
    private Servicio servicio;

    public ServicioRepository(Application application){
        AppDatabase db = AppDatabase.getInstance(application);
        servicioDao = db.servicioDao();
        servicios = servicioDao.getAll();
    }

    public LiveData<List<Servicio>> getServicio(){
        return servicios;
    }

    public void insert(Servicio servicio){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            servicioDao.insert(servicio);
        });
    }

    public void update(Servicio servicio){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            servicioDao.update(servicio);
        });
    }

    public void delete(Servicio servicio){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            servicioDao.delete(servicio);
        });
    }

}
