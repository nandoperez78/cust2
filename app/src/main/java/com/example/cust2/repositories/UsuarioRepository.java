package com.example.cust2.repositories;

       // import androidx.annotation.WorkerThread;
        import android.app.Application;
        import androidx.lifecycle.LiveData;
        import com.example.cust2.daos.UsuarioDao;
        import com.example.cust2.database.AppDatabase;
        import com.example.cust2.entities.Usuario;

        import java.util.List;

public class UsuarioRepository {
    private UsuarioDao usuarioDao;

    private LiveData<List<Usuario>> usuarios;
    private Usuario usuario;

    public UsuarioRepository(Application application){
        AppDatabase db = AppDatabase.getInstance(application);
        usuarioDao = db.usuarioDao();
        usuarios = usuarioDao.getAll();
    }

    public LiveData<List<Usuario>> getUsuario(){
        return usuarios;
    }

    public void insert(Usuario usuario){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            usuarioDao.insert(usuario);
        });
    }

    public void update(Usuario usuario){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            usuarioDao.update(usuario);
        });
    }

    public void delete(Usuario usuario){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            usuarioDao.delete(usuario);
        });
    }

    public Usuario login(String user, String password){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            usuario = usuarioDao.login(user, password);
        });
        return usuario;
    }
}

