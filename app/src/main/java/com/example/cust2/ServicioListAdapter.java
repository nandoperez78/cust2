package com.example.cust2;

import android.net.sip.SipSession;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.cust2.entities.Servicio;

import java.util.List;

public class ServicioListAdapter extends ListAdapter<Servicio,ServicioViewHolder> {

    private OnItemClickListener listener;

    public ServicioListAdapter(@NonNull DiffUtil.ItemCallback<Servicio> diffCallbak){
        super(diffCallbak);
    }

    @NonNull
    @Override
    public ServicioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ServicioViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicioViewHolder holder, int position) {
        Servicio servicioActual = getItem(position);
        holder.bind(servicioActual.getNombre(), servicioActual.getDescripcion());

        ImageButton deleteButton = holder.itemView.findViewById(R.id.imageButtonDelete);
        deleteButton.setOnClickListener(View -> {
            if(listener!=null){
                listener.onItemDelete(servicioActual);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(servicioActual);
                }
            }
        });
    }

    static class ServicioDiff extends DiffUtil.ItemCallback<Servicio>{
        @Override
        public boolean areItemsTheSame(@NonNull Servicio oldItem, @NonNull Servicio newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Servicio oldItem, @NonNull Servicio newItem) {
            return oldItem.getNombre().equals(newItem.getNombre()) && oldItem.getDescripcion().equals(newItem.getDescripcion());
        }
    }

    public interface OnItemClickListener {
        void onItemDelete(Servicio servicio);
        void onItemClick(Servicio servicio);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
