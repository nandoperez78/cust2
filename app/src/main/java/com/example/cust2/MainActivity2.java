package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    public CardView card_view_Colaboradores;
    public CardView card_view_Clientes;
    public CardView card_view_Servicios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        card_view_Colaboradores = (CardView) findViewById(R.id.card_view_Colaboradores);
        card_view_Colaboradores.setOnClickListener(this);

        card_view_Clientes = (CardView) findViewById(R.id.card_view_Clientes);
        card_view_Clientes.setOnClickListener(this);

        card_view_Servicios = (CardView) findViewById(R.id.card_view_Servicios);
        card_view_Servicios.setOnClickListener(this);
    }

    public void MainActivity(View view){
        Intent mainactivity = new Intent(this, MainActivity.class);
        startActivity(mainactivity);
    }

    @Override
    public void onClick(View v) {
        Intent i;

        switch (v.getId()) {
            case R.id.card_view_Colaboradores :
                i = new Intent(this, ListaUsuarioActivity.class);
                startActivity(i);
                break;

            case R.id.card_view_Clientes :
                i = new Intent(this, ListaClienteActivity.class);
                startActivity(i);
                break;

            case R.id.card_view_Servicios:
                i = new Intent(this, ListaServicioActivity.class);
                startActivity(i);
                break;
        }
    }
}
