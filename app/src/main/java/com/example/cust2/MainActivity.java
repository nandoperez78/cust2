package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.cust2.entities.Usuario;
import com.example.cust2.models.UsuarioViewModel;


public class MainActivity extends AppCompatActivity {
    private EditText editTextUsuario;
    private EditText editTextPassword;
    private Button buttonLogin;
    private TextView buttonRegistro;
    private TextView textViewError;

    public static final int NEW_USUARIO_REQ_CODE = 1;

    private UsuarioViewModel usuarioViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextUsuario = findViewById(R.id.textViewUsuario);
        editTextPassword = findViewById(R.id.textViewPassword);
        buttonLogin = findViewById(R.id.buttonLogin);
        textViewError = findViewById(R.id.textViewError);
        buttonRegistro = findViewById(R.id.buttonRegistro);

        usuarioViewModel = new ViewModelProvider(this, new UsuarioFactory(getApplication())).get(UsuarioViewModel.class);

        buttonLogin.setOnClickListener(view -> {
            try {
                Usuario usuario = usuarioViewModel.login(editTextUsuario.getText().toString(), editTextPassword.getText().toString());
                if (usuario != null) {
                    Intent intent = new Intent(this, MainActivity2.class);
                    startActivity(intent);
                } else {
                    textViewError.setText("Error de usuario o contraseña");
                }
            } catch (Exception e) {
                textViewError.setText(e.getMessage());
            }
        });

        buttonRegistro.setOnClickListener(view -> {
            Intent intent = new Intent(this, AgregarUsuarioActivity.class);
            startActivityForResult(intent, NEW_USUARIO_REQ_CODE);
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_USUARIO_REQ_CODE && resultCode == RESULT_OK) {
            try {
                Usuario usuario = new Usuario();
                usuario.setNombre(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_NOMBRE_USU));
                usuario.setTelefono(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_TELEFONO));
                usuario.setDireccion(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_DIRECCION));
                usuario.setCorreo(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_CORREO));
                usuario.setCedula(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_CEDULA));
                usuario.setSexo(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_SEXO));
                usuario.setObservaciones(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_OBSERVACIONES));
                usuario.setPassword(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_PASSWORD));
                usuarioViewModel.insert(usuario);
                textViewError.setText("Nuevo Usuario Ingresado.");
            } catch (Exception e) {
                textViewError.setText(e.getMessage());
            }
        }
    }
}
