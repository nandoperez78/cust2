package com.example.cust2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cust2.entities.Usuario;
import com.example.cust2.models.UsuarioViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ListaUsuarioActivity extends AppCompatActivity {

    private UsuarioViewModel usuarioViewModel;
    public static final int NEW_USUARIO_REQ_CODE = 1;
    public static final int UPDATE_USUARIO_REQ_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_usuario);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewUsuarios);
        final UsuarioListAdapter adapter = new UsuarioListAdapter(new UsuarioListAdapter.UsuarioDiff());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        usuarioViewModel = new ViewModelProvider(this, new UsuarioFactory(getApplication())).get(UsuarioViewModel.class);

        usuarioViewModel.getUsuario().observe(this, usuarios -> {
            adapter.submitList(usuarios);
        });

        FloatingActionButton fab = findViewById(R.id.btnAgregar);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(ListaUsuarioActivity.this, AgregarUsuarioActivity.class);

            startActivityForResult(intent, NEW_USUARIO_REQ_CODE);
        });

        adapter.setOnItemClickListener(new UsuarioListAdapter.OnItemClickListener() {
            @Override
            public void onItemDelete(Usuario usuario) {
                usuarioViewModel.delete(usuario);
                Toast.makeText(getApplicationContext(), R.string.borrado, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onItemClick(Usuario usuario) {
                Intent intent = new Intent(ListaUsuarioActivity.this, AgregarUsuarioActivity.class);
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_NOMBRE_USU, usuario.getNombre());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_FECHA_NACIMIENTO, usuario.getFecha_nacimiento());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_ID, usuario.getId());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_DIRECCION, usuario.getDireccion());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_TELEFONO, usuario.getTelefono());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_CORREO, usuario.getCorreo());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_CEDULA, usuario.getCedula());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_SEXO, usuario.getSexo());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_OBSERVACIONES, usuario.getObservaciones());
                intent.putExtra(AgregarUsuarioActivity.EXTRA_MSG_PASSWORD, usuario.getPassword());
                startActivityForResult(intent, UPDATE_USUARIO_REQ_CODE);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==NEW_USUARIO_REQ_CODE && resultCode == RESULT_OK){
            Usuario usuario = new Usuario();
            usuario.setNombre(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_NOMBRE_USU));
            usuario.setFecha_nacimiento(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_FECHA_NACIMIENTO));
            usuario.setDireccion(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_DIRECCION));
            usuario.setTelefono(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_TELEFONO));
            usuario.setCorreo(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_CORREO));
            usuario.setCedula(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_CEDULA));
            usuario.setSexo(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_SEXO));
            usuario.setObservaciones(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_OBSERVACIONES));
            usuario.setPassword(data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_PASSWORD));
            usuarioViewModel.insert(usuario);
        } else if (requestCode == UPDATE_USUARIO_REQ_CODE && resultCode == RESULT_OK){
            int id = data.getIntExtra(AgregarUsuarioActivity.EXTRA_MSG_ID, -1);
            if (id == -1){
                Toast.makeText(getApplicationContext(), R.string.no_actualizado, Toast.LENGTH_LONG).show();
            }
            String nombre = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_NOMBRE_USU);
            String fecha_nacimiento = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_FECHA_NACIMIENTO);
            String direccion = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_DIRECCION);
            String telefono = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_TELEFONO);
            String correo = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_CORREO);
            String cedula = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_CEDULA);
            String sexo = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_SEXO);
            String observaciones = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_OBSERVACIONES);
            String password = data.getStringExtra(AgregarUsuarioActivity.EXTRA_MSG_PASSWORD);

            Usuario usuario = new Usuario(id, nombre, fecha_nacimiento, direccion, telefono, correo, cedula, sexo, observaciones, password);
            usuarioViewModel.update(usuario);

        } else {
            Toast.makeText(getApplicationContext(), R.string.no_guardado, Toast.LENGTH_LONG).show();
        }
    }

}
