package com.example.cust2;


import android.net.sip.SipSession;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.cust2.entities.Cliente;

import java.util.List;

public class ClienteListAdapter extends ListAdapter<Cliente,ClienteViewHolder> {

    private OnItemClickListener listener;

    public ClienteListAdapter(@NonNull DiffUtil.ItemCallback<Cliente> diffCallbak){
        super(diffCallbak);
    }

    @NonNull
    @Override
    public ClienteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ClienteViewHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ClienteViewHolder holder, int position) {
        Cliente clienteActual = getItem(position);
        holder.bind(clienteActual.getNombre());

        ImageButton deleteButton = holder.itemView.findViewById(R.id.imageButtonDelete);
        deleteButton.setOnClickListener(View -> {
            if(listener!=null){
                listener.onItemDelete(clienteActual);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.onItemClick(clienteActual);
                }
            }
        });
    }

    static class ClienteDiff extends DiffUtil.ItemCallback<Cliente>{
        @Override
        public boolean areItemsTheSame(@NonNull Cliente oldItem, @NonNull Cliente newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Cliente oldItem, @NonNull Cliente newItem) {
            return oldItem.getNombre().equals(newItem.getNombre()) && oldItem.getFecha_nacimiento().equals(newItem.getFecha_nacimiento())&& oldItem.getDireccion().equals(newItem.getDireccion())&& oldItem.getTelefono().equals(newItem.getTelefono())&& oldItem.getCorreo().equals(newItem.getCorreo())&& oldItem.getCedula().equals(newItem.getCedula())&& oldItem.getSexo().equals(newItem.getSexo())&& oldItem.getObservaciones().equals(newItem.getObservaciones());
        }
    }

    public interface OnItemClickListener {
        void onItemDelete(Cliente cliente);
        void onItemClick(Cliente cliente);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}

