package com.example.cust2.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.cust2.daos.ClienteDao;
import com.example.cust2.daos.ServicioDao;
import com.example.cust2.daos.UsuarioDao;
import com.example.cust2.entities.Cliente;
import com.example.cust2.entities.Servicio;
import com.example.cust2.entities.Usuario;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Usuario.class, Cliente.class, Servicio.class}, version = 5)
    public abstract class AppDatabase extends RoomDatabase {
    public abstract UsuarioDao usuarioDao();
    public abstract ClienteDao clienteDao();
    public abstract ServicioDao servicioDao();

    private static volatile AppDatabase instance;

    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(4);
    public static AppDatabase getInstance( final Context context){
        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "custdb")
                        .fallbackToDestructiveMigration()
                        .build();
        }
        return instance;
    }
}
